package org.hexlet.java.course1.tasks;

/**
 * Created by sergey on 5/10/14.
 */
public class Field {
    private char[][] field;
    private int sizeOfField;
    private char emptyChar = ' ';

    public Field() {
        this(3);
    }

    public Field(int sizeOfField) {
        this.sizeOfField = sizeOfField;
        initField();
    }

    private void initField() {
        field = new char[sizeOfField][sizeOfField];
        setFieldEmpty();
    }

    private void setFieldEmpty() {
        for (int i = 0; i < sizeOfField; i++) {
            for (int j = 0; j < sizeOfField; j++) {
                field[i][j] = emptyChar;
            }
        }
    }

    public void printField() {
        for (int i = 0; i < sizeOfField; i++) {
            for (int j = 0; j < sizeOfField; j++) {
                System.out.print("[" + field[i][j] + "]");
            }
            System.out.println();
        }

    }
}
